package com.example.listview;
import android.content.Context;
import android.media.Image;
import android.service.autofill.UserData;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
public class SvAdapter  extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Sinhvien> sinhvienList;
    public SvAdapter(Context context, int layout, List<Sinhvien> sinhvienList) {
        this.context = context;
        this.layout = layout;
        this.sinhvienList = sinhvienList;  }
    @Override
    public int getCount() {
        return sinhvienList.size();    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(layout,null);
        //Ánh xạ view
        TextView txtten = (TextView) convertView.findViewById(R.id.ten);
        TextView txtmsv = (TextView) convertView.findViewById(R.id.msv);
        ImageView hinh =(ImageView) convertView.findViewById(R.id.hinh);
        TextView diem = (TextView) convertView.findViewById(R.id.diem);
        TextView lop = (TextView) convertView.findViewById(R.id.lop);
        TextView nienkhoa = (TextView)convertView.findViewById(R.id.nienkhoa);
        Sinhvien sinhvien = sinhvienList.get(position);
        txtten.setText(sinhvien.getName());
        txtmsv.setText(sinhvien.getMsv());
        hinh.setImageResource(sinhvien.getHinh());
        return convertView;
    }
}

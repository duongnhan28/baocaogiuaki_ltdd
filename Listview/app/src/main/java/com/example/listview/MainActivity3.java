package com.example.listview;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity3 extends AppCompatActivity {
    private ListView lvsinhvien;
    ArrayList<Sinhvien> sinhvienArrayList = new ArrayList<Sinhvien>();
    SvAdapter svAdapter;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        lvsinhvien = (ListView) findViewById(R.id.listviewsvv);
        String  username = getIntent().getExtras().getString("username");
        String  password = getIntent().getExtras().getString("password");

        sinhvienArrayList.add(new Sinhvien("1811505310136","Dương Văn Nhân",R.drawable.anh,username,password,"7.2","18t1","2018-2022"));
        sinhvienArrayList.add(new Sinhvien("1811505310137","Dương Văn Quyền",R.drawable.anh,"quyen","1","7.2","18t1","2018-2022"));
        sinhvienArrayList.add(new Sinhvien("1811505310138","Nguyễn Đông Phương",R.drawable.anh,"phuong","1","7.2","18t1","2018-2022"));
        sinhvienArrayList.add(new Sinhvien("1811505310139","Trần Hoàng Chung",R.drawable.anh,"chung","1","7.2","18t1","2018-2022"));
        sinhvienArrayList.add(new Sinhvien("1811505310140","Võ Ngọc Trãi",R.drawable.anh,"trai","1","7.2","18t1","2018-2022"));
        sinhvienArrayList.add(new Sinhvien("1811505310141","Hoàng Văn Linh",R.drawable.anh,"linh","1","7.2","18t1","2018-2022"));
        sinhvienArrayList.add(new Sinhvien("1811505310142","Nguyễn Văn A",R.drawable.anh,"a","1","7.2","18t1","2018-2022"));
        svAdapter = new SvAdapter(this,R.layout.dong_sv,sinhvienArrayList);
        lvsinhvien.setAdapter(svAdapter);
        lvsinhvien.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Showitem(position);
                Toast.makeText(MainActivity3.this,"Tài khoản:" +sinhvienArrayList.get(position).getUsername() + " "
                       +"Mật khẩu:" +sinhvienArrayList.get(position).getPassword(),Toast.LENGTH_LONG).show();
            }
            private void Showitem(int position) {
                Sinhvien sv= sinhvienArrayList.get(position);
                Intent i = new Intent(MainActivity3.this, Detail.class);
                i.putExtra("Tên",sv.getName());
                i.putExtra("Msv",sv.getMsv());
                i.putExtra("diem",sv.getDiemtongket());
                i.putExtra("lop",sv.getLop());
                i.putExtra("nienkhoa",sv.getNienkhoa());
                i.putExtra("username",sv.getUsername());
                i.putExtra("password",sv.getPassword());
                startActivity(i);
            }
        });
    }

}
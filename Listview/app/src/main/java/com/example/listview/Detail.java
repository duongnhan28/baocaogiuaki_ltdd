package com.example.listview;
import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
public class Detail extends AppCompatActivity {
    TextView name,msv,diemm,lopp,nkk,username,password;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        name = (TextView)findViewById(R.id.name);
        msv = (TextView)findViewById(R.id.msv);
        diemm = (TextView)findViewById(R.id.diem);
        lopp = (TextView)findViewById(R.id.lop);
        nkk = (TextView)findViewById(R.id.nienkhoa);
        username= (TextView)findViewById(R.id.username);
        password = (TextView)findViewById(R.id.password);
        img=(ImageView)findViewById(R.id.imag);
        String ten= getIntent().getStringExtra("Tên");
        String masv = getIntent().getStringExtra("Msv");
        String diem = getIntent().getStringExtra("diem");
        String lop = getIntent().getStringExtra("lop");
        String nk = getIntent().getStringExtra("nienkhoa");
        String user = getIntent().getStringExtra("username");
        String pass = getIntent().getStringExtra("password");
        name.setText("Tên: "+ ten);
        msv.setText("Mã sinh viên: "+masv);
        diemm.setText("Điểm: "+diem);
        lopp.setText("Lớp: "+lop);
        nkk.setText("Niên khóa: "+nk);
        username.setText("Username: "+user);
        password.setText("Password: "+pass);
        img.setImageResource(R.drawable.anh);
    }
}
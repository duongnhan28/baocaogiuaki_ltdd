package com.example.listview;

import java.util.ArrayList;

public class Sinhvien extends ArrayList<String> {
    private String msv;
    private String name;
    private int hinh;
    private String username,password,diemtongket,lop,nienkhoa;

    public Sinhvien(String msv, String name, int hinh,String username,String password,String diemtongket,String lop,String nienkhoa) {
        this.msv = msv;
        this.name = name;
        this.lop = lop;
        this.hinh = hinh;
        this.username=username;
        this.password = password;
        this.diemtongket = diemtongket;
        this.nienkhoa=nienkhoa;
    }
    public Sinhvien(String username,String password,String msv, String name){
        this.username=username;
        this.password = password;
        this.msv = msv;
        this.name = name;
    }

    public String getMsv() {
        return msv;
    }

    public void setMsv(String msv) {
        this.msv = msv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHinh() {
        return hinh;
    }

    public void setHinh(int hinh) {
        this.hinh = hinh;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDiemtongket() {
        return diemtongket;
    }

    public void setDiemtongket(String diemtongket) {
        this.diemtongket = diemtongket;
    }

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }

    public String getNienkhoa() {
        return nienkhoa;
    }

    public void setNienkhoa(String nienkhoa) {
        this.nienkhoa = nienkhoa;
    }
}

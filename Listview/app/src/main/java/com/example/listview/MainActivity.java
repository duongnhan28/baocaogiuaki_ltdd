package com.example.listview;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.Inet4Address;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private EditText edtuser,edtpassword;
    private Button btndangky,btndangnhap;
    public String username,password;
    public ArrayList<Sinhvien> sinhvienArrayList = new ArrayList<Sinhvien>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtuser = (EditText)findViewById(R.id.inputUsername);
        edtpassword = (EditText)findViewById(R.id.inputPassword);
        btndangky = (Button)findViewById(R.id.btndangky);
        btndangnhap = (Button)findViewById(R.id.btnlogin);
        sinhvienArrayList.add(new Sinhvien("quyen","1","1811505310137","Dương Văn Quyền"));
        sinhvienArrayList.add(new Sinhvien("phuong","1","1811505310138","Nguyễn Đông Phương"));
        sinhvienArrayList.add(new Sinhvien("chung","1","1811505310139","Trần Hoàng Chung"));
        sinhvienArrayList.add(new Sinhvien("trai","1","18115053101310","Võ Ngọc Trãi"));
        sinhvienArrayList.add(new Sinhvien("linh","1",  "18115053101311","Hoàng Văn Linh"));
        ControllerButton();
    }
    private void ControllerButton() {
        btndangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle("Đăng ký");
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.customdialog);
                EditText edttk =(EditText)dialog.findViewById(R.id.edtdk);
                EditText edtmk =(EditText)dialog.findViewById(R.id.edtmk);
                EditText edtmsv = (EditText)dialog.findViewById(R.id.edtmasv);
                EditText edtname = (EditText)dialog.findViewById(R.id.edtname);
                Button btnhuy = (Button)dialog.findViewById(R.id.buttonhuy);
                Button btndongy =(Button)dialog.findViewById(R.id.buttondongy);
                btndongy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sinhvienArrayList.add(new Sinhvien(
                                edttk.getText().toString(),
                                edtmk.getText().toString(),
                                edtmsv.getText().toString(),
                                edtname.getText().toString()));
                        Toast.makeText(MainActivity.this,"Đăng ký thành công",Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });
                    btnhuy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });
                    dialog.show();
            }
        });
        btndangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtuser.getText().length() != 0 && edtpassword.getText().length() !=0){
                    if (Check(sinhvienArrayList,edtuser.getText().toString(),edtpassword.getText().toString())==true){
                        Toast.makeText(MainActivity.this,"Đăng nhập thành công",Toast.LENGTH_SHORT).show();
                        username = edtuser.getText().toString();
                        password = edtpassword.getText().toString();
                        Bundle bundle = new Bundle();
                        bundle.putString("username", username);
                        bundle.putString("password", password);
                        Intent intent = new Intent(MainActivity.this,MainActivity3.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }else
                    Toast.makeText(MainActivity.this,"Đăng nhập thất bại",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public boolean Check(ArrayList<Sinhvien> data, String username, String password) {
        for (Sinhvien i : data) {
            if (i.getPassword().compareTo(password)==0 && i.getUsername().equalsIgnoreCase(username)) {
                return true;
            }
        }
        return false;
    }
}